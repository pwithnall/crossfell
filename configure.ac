AC_PREREQ(2.65)

# Crossfell release version
m4_define(cfl_version_major, 0)
m4_define(cfl_version_minor, 1)
m4_define(cfl_version_micro, 0)

# libgpx release version
m4_define(gpx_version_major, 0)
m4_define(gpx_version_minor, 1)
m4_define(gpx_version_micro, 0)

# libgpx API version
m4_define(gpx_api_version_major, 0)
m4_define(gpx_api_version_minor, 0)

AC_INIT([crossfell],[cfl_version_major.cfl_version_minor.cfl_version_micro],[https://bugzilla.gnome.org/enter_bug.cgi?product=crossfell],[crossfell],[http://live.gnome.org/crossfell])

AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_SRCDIR([crossfell/main.c])
AC_CONFIG_HEADERS([config.h])
AC_USE_SYSTEM_EXTENSIONS

AM_INIT_AUTOMAKE([1.11 dist-bzip2 no-dist-gzip check-news subdir-objects])
AM_SILENT_RULES([yes])

AC_PROG_CXX
AM_PROG_CC_C_O
LT_INIT([])
PKG_PROG_PKG_CONFIG

# Requirements
GLIB_REQS=2.31.0
GIO_REQS=2.17.3
GTK_REQS=3.9.0
CHAMPLAIN_REQS=0.12
CHAMPLAIN_GTK_REQS=0.12

# Crossfell versioning
CFL_VERSION_MAJOR=cfl_version_major
CFL_VERSION_MINOR=cfl_version_minor
CFL_VERSION_MICRO=cfl_version_micro
AC_SUBST([CFL_VERSION_MAJOR])
AC_SUBST([CFL_VERSION_MINOR])
AC_SUBST([CFL_VERSION_MICRO])

# Crossfell dependencies
CFL_PACKAGES_PUBLIC="gobject-2.0 glib-2.0 >= $GLIB_REQS gio-2.0 >= $GIO_REQS gtk+-3.0 >= $GTK_REQS champlain-0.12 >= $CHAMPLAIN_REQS champlain-gtk-0.12 >= $CHAMPLAIN_GTK_REQS"
CFL_PACKAGES_PRIVATE="gthread-2.0"
CFL_PACKAGES="$CFL_PACKAGES_PUBLIC $CFL_PACKAGES_PRIVATE"
AC_SUBST([CFL_PACKAGES_PUBLIC])
AC_SUBST([CFL_PACKAGES_PRIVATE])
AC_SUBST([CFL_PACKAGES])

PKG_CHECK_MODULES(CFL, [$CFL_PACKAGES])

# libgpx versioning
#
# Before making a release, the GPX_LT_VERSION string should be modified. The string is of the form c:r:a. Follow these instructions sequentially:
#
#  1. If the library source code has changed at all since the last update, then increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#  2. If any interfaces have been added, removed, or changed since the last update, increment current, and set revision to 0.
#  3. If any interfaces have been added since the last public release, then increment age.
#  4. If any interfaces have been removed or changed since the last public release, then set age to 0.
GPX_LT_VERSION=0:0:0
AC_SUBST([GPX_LT_VERSION])

GPX_VERSION=gpx_version_major.gpx_version_minor.gpx_version_micro
GPX_VERSION_MAJOR=gpx_version_major
GPX_VERSION_MINOR=gpx_version_minor
GPX_VERSION_MICRO=gpx_version_micro
GPX_API_VERSION=gpx_api_version_major.gpx_api_version_minor
GPX_API_VERSION_U=gpx_api_version_major[_]gpx_api_version_minor
GPX_API_VERSION_MAJOR=gpx_api_version_major
GPX_API_VERSION_MINOR=gpx_api_version_minor
AC_SUBST([GPX_VERSION])
AC_SUBST([GPX_VERSION_MAJOR])
AC_SUBST([GPX_VERSION_MINOR])
AC_SUBST([GPX_VERSION_MICRO])
AC_SUBST([GPX_API_VERSION])
AC_SUBST([GPX_API_VERSION_U])
AC_SUBST([GPX_API_VERSION_MAJOR])
AC_SUBST([GPX_API_VERSION_MINOR])

# libgpx dependencies
GPX_PACKAGES_PUBLIC=""
GPX_PACKAGES_PRIVATE="libxml-2.0"
GPX_PACKAGES="$GPX_PACKAGES_PUBLIC $GPX_PACKAGES_PRIVATE"
AC_SUBST([GPX_PACKAGES_PUBLIC])
AC_SUBST([GPX_PACKAGES_PRIVATE])
AC_SUBST([GPX_PACKAGES])

PKG_CHECK_MODULES(GPX, [$GPX_PACKAGES])

# libgd
LIBGD_INIT([main-view])

# Internationalisation
GETTEXT_PACKAGE=AC_PACKAGE_NAME
AC_SUBST([GETTEXT_PACKAGE])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE],"$GETTEXT_PACKAGE",[The name of the gettext domain])
IT_PROG_INTLTOOL([0.40.0])

# Code coverage
GNOME_CODE_COVERAGE

# gtk-doc
GTK_DOC_CHECK([1.14],[--flavour no-tmpl])

# General GNOME macros
GNOME_DEBUG_CHECK
GNOME_COMPILE_WARNINGS([maximum])
GNOME_MAINTAINER_MODE_DEFINES

AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_CXXFLAGS])
AC_SUBST([AM_LDFLAGS])

AC_CONFIG_FILES([
Makefile
libgd/Makefile
libgpx/libgpx-$GPX_API_VERSION.pc:libgpx/libgpx.pc.in
libgpx/gpx-version.h
libgpx/docs/Makefile
libgpx/docs/version.xml
libgpx/tests/Makefile
po/Makefile.in
],[],
[GPX_API_VERSION='$GPX_API_VERSION'])
AC_OUTPUT
