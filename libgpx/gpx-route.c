/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:gpx-route
 * @short_description: a GPX route
 * @stability: Unstable
 * @include: libgpx/gpx-route.h
 *
 * TODO: Write more documentation. Describe differences between routes and
 * tracks.
 *
 * Since: 0.1.0
 */

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "gpx-route.h"
#include "gpx-waypoint.h"

/* Private route members. */
struct _GpxRoute {
	/* When this reaches 0, the GpxRoute is destroyed. */
	unsigned int ref_count;

	/* TODO */
};

/**
 * gpx_route_new:
 *
 * Create a new, empty GPX route.
 *
 * If memory allocation fails, %NULL is returned.
 *
 * Return value: (transfer full) (allow-none): newly allocated #GpxRoute,
 * or %NULL; unref using gpx_route_unref()
 *
 * Since: 0.1.0
 */
GpxRoute *
gpx_route_new (void)
{
	GpxRoute *route;

	route = malloc (sizeof (GpxRoute));
	if (route == NULL) {
		/* Allocation failed. */
		return NULL;
	}

	/* Initialise all fields. */
	memset(route, 0, sizeof (*route));
	route->ref_count = 1;

	return route;
}

static void _gpx_route_destroy (GpxRoute *route) __attribute__((nonnull));

/* Destroy the given route. */
static void
_gpx_route_destroy (GpxRoute *route)
{
	free (route);
}

/**
 * gpx_route_ref:
 * @self: a #GpxRoute
 *
 * Increment the reference count of the given #GpxRoute.
 * TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_route_ref (GpxRoute *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	self->ref_count++;
}

/**
 * gpx_route_unref:
 * @self: a #GpxRoute
 *
 * Decrement the reference count of the given #GpxRoute. If the reference count
 * reaches 0, the #GpxRoute is deallocated.
 *
 * TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_route_unref (GpxRoute *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);

	self->ref_count--;
	if (self->ref_count == 0) {
		/* Destroy the route. */
		_gpx_route_destroy (self);
	}
}

/**
 * gpx_route_get_bounding_box:
 * @self: a #GpxRoute
 * @bounding_box: (out caller-allocates): return location TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_route_get_bounding_box (const GpxRoute *self, GpxBoundingBox *bounding_box)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (bounding_box != NULL);

	/* TODO */
	bounding_box->left = 0.0;
	bounding_box->right = 0.0;
	bounding_box->top = 0.0;
	bounding_box->bottom = 0.0;
}

/**
 * gpx_route_append_waypoint:
 * @self: a #GpxRoute
 * @waypoint: TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_route_append_waypoint (GpxRoute *self, const GpxWaypoint *waypoint)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (waypoint != NULL);

	/* TODO */
}

/**
 * gpx_route_get_waypoints:
 * @self: a #GpxRoute
 * @n_waypoints: (out): TODO
 *
 * TODO
 *
 * Return value: (transfer none) (array length=n_waypoints): TODO
 *
 * Since: 0.1.0
 */
const GpxWaypoint * const *
gpx_route_get_waypoints (const GpxRoute *self, unsigned int *n_waypoints)
{
	/* TODO */
	return NULL;
}

