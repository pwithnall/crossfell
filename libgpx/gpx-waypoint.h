/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:gpx-waypoint
 * @short_description: a GPX waypoint
 * @stability: Unstable
 * @include: libgpx/gpx-waypoint.h
 *
 * TODO: Write more documentation. Describe differences between waypoints and
 * points.
 *
 * Since: 0.1.0
 */

#ifndef GPX_WAYPOINT_H
#define GPX_WAYPOINT_H

#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * GpxWaypoint:
 * @latitude: TODO
 * @longitude: TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
typedef struct {
	double latitude;
	double longitude;
} GpxWaypoint;

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* !GPX_WAYPOINT_H */
